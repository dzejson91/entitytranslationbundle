# Symfony Translation Entity Bundle for Doctrine ORM

## Install
`composer install dzejson91/entitytranslationbundle`

## Configure Bundle
Register installed bundle in AppKernel:
```php
$bundles = array(
    [...]
    new TranslationEntityBundle(),
);
```

## Configure Entities
+ Create entity (platform ORM)
+ Your Entity must implements EntityTranslatableInterface and use EntityTranslatableTrait.
+ Return in function getTranslatableClass() name of class witch will be translation for entity
```php
[...]
/**
 * @ORM\Entity()
 * @ORM\Table(name="blog_items")
 */
class BlogItem implements EntityTranslatableInterface
{
    use EntityTranslatableTrait;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column()
     */
    protected $title;

    /**
     * @ORM\Column(type=text)
     */
    protected $description;
[...]
    public static function getTranslatableClass(){
        return BlogItemTrans::class;
    }
[...]
```
+ Create translation class for entity
+ Translation class must extends AbstractEntityTranslation
+ Define properties witch you want to be translated (properties must exists in main entity class)
```php
[...]
/**
 * @ORM\Entity()
 * @ORM\Table(name="blog_items_trans")
 */
class BlogItemsTrans extends AbstractEntityTranslation
{
    protected $title;
    protected $description;
}
```

+ update doctrine schema
```bash
php bin/console doctrine:schema:update --dump-sql --force
```

## Configure Locales
```php
/** @var TranslatableSubscriber $ts */ 
$ts = $container->get('entity_translation_subscriber');
$ts->setDefaultLocale('en_US');
$ts->setCurrentocale('pl_PL');
```
## Query

Entity Repository functions
```php
/** @var QueryBuilder $qb */
$qb = $this->createQueryBuilder('b')
    ->where('b.slug = :slug')
    ->setParameter('slug', 'new-post')
;

/** @var Query $query */
$query = $qb->getQuery();
$query->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class);
$query->setHint(TranslatableSubscriber::HINT_FALLBACK, true); // optional
$query->setHint(TranslatableSubscriber::HINT_LOCALE, 'pl_PL');// optional

$results = $query->getResult();
return $results;
```

## Insert & Update

Insert and Update work automatically.