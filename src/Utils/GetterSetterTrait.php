<?php
/**
 * Created by PhpStorm.
 * User: Krystian
 * Date: 09.11.2018
 * Time: 22:08
 */

namespace TranslationEntityBundle\Utils;

trait GetterSetterTrait
{
    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws
     */
    public function __call($name, $arguments)
    {
        if(preg_match('/^(get|is|set)[A-Z]/', $name, $match)) {

            $propertyName = lcfirst(substr($name, strlen($match[1])));
            $propertyExists = isset($this->$propertyName);

            if($match[1] == 'is'){
                return $propertyExists;
            }

            if(!$propertyExists){
                throw new \Exception(sprintf('Property %s doesn\'t exists', $propertyName));
            }

            if($match['1'] == 'get'){
                return $this->$propertyName;
            } else {
                $this->$propertyName = reset($arguments);
            }

        } else {
            throw new \BadMethodCallException(sprintf('Method $s doesn\'t exists', $name));
        }
        return null;
    }

}