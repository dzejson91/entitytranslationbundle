<?php
/**
 * Created by PhpStorm.
 * User: Krystian
 * Date: 26.09.2018
 * Time: 20:08
 */

namespace TranslationEntityBundle\Translation;

use Doctrine\Common\Collections\ArrayCollection;

interface EntityTranslatableInterface
{
    /**
     * @return string
     */
    public static function getTranslatableClass();

    /**
     * @return ArrayCollection
     */
    public function getTranslations();

    /**
     * @param ArrayCollection $translations
     */
    public function setTranslations($translations);

    /**
     * @param AbstractEntityTranslation $translation
     */
    public function addTranslation(AbstractEntityTranslation $translation);

    /**
     * @param AbstractEntityTranslation $translation
     */
    public function removeTranslation(AbstractEntityTranslation $translation);

    /**
     * @param string $locale
     * @return bool
     */
    public function hasTranslationLocale($locale);

    /**
     * @param string $locale
     * @return AbstractEntityTranslation
     */
    public function getTranslationByLocale($locale);
}