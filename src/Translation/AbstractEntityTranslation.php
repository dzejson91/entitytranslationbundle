<?php
/**
 * Created by PhpStorm.
 * User: Krystian
 * Date: 05.11.2018
 * Time: 22:55
 */

namespace TranslationEntityBundle\Translation;

use Doctrine\ORM\Mapping as ORM;
use TranslationEntityBundle\Utils\GetterSetterTrait;

/**
 * Class AbstractEntityTranslation
 * @package TranslationEntityBundle\Translation
 *
 * @ORM\MappedSuperclass()
 */
abstract class AbstractEntityTranslation
{
    use GetterSetterTrait;

    /**
     * @var bool
     */
    protected static $fallback = true;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $transId;

    /**
     * @var \stdClass
     */
    protected $transObject;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=10)
     */
    protected $transLocale;

    /**
     * @return int
     */
    public function getTransId()
    {
        return $this->transId;
    }

    /**
     * @return mixed
     */
    public function getTransObject()
    {
        return $this->transObject;
    }

    /**
     * @param mixed $transObject
     */
    public function setTransObject($transObject)
    {
        $this->transObject = $transObject;
    }

    /**
     * @return mixed
     */
    public function getTransLocale()
    {
        return $this->transLocale;
    }

    /**
     * @param mixed $transLocale
     */
    public function setTransLocale($transLocale)
    {
        $this->transLocale = $transLocale;
    }

    /**
     * @return bool
     */
    public static function isFallback()
    {
        $class = get_called_class();
        return $class::$fallback;
    }
}