<?php
/**
 * Created by PhpStorm.
 * User: Krystian
 * Date: 05.11.2018
 * Time: 22:47
 */

namespace TranslationEntityBundle\Translation;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

trait EntityTranslatableTrait
{
    /** @var ArrayCollection */
    protected $translations;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param ArrayCollection $translations
     */
    public function setTranslations($translations)
    {
        $this->translations = $translations;
    }

    /**
     * @param AbstractEntityTranslation $translation
     */
    public function addTranslation(AbstractEntityTranslation $translation)
    {
        if(!$this->translations->contains($translation)){
            $this->translations->add($translation);
            $translation->setTransObject($this);
        }
    }

    /**
     * @param AbstractEntityTranslation $translation
     */
    public function removeTranslation(AbstractEntityTranslation $translation)
    {
        if($this->translations->contains($translation)){
            $this->translations->removeElement($translation);
        }
    }

    /**
     * @param string $locale
     * @return bool
     */
    public function hasTranslationLocale($locale)
    {
        $expression = Criteria::expr();
        $criteria = Criteria::create()->where($expression->eq('transLocale', $locale));
        $results = $this->translations->matching($criteria);
        return !$results->isEmpty();
    }

    /**
     * @param string $locale
     * @return AbstractEntityTranslation
     */
    public function getTranslationByLocale($locale)
    {
        $expression = Criteria::expr();
        $criteria = Criteria::create()->where($expression->eq('transLocale', $locale));
        $results = $this->translations->matching($criteria);
        return $results->first();
    }
}