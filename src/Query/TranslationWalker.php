<?php

namespace TranslationEntityBundle\Query;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\SqlWalker;
use Doctrine\ORM\Query\AST\SelectStatement;
use Doctrine\ORM\Query\Exec\SingleSelectExecutor;
use Doctrine\ORM\Query\AST\RangeVariableDeclaration;
use Doctrine\ORM\Query\AST\Join;
use TranslationEntityBundle\EventListener\TranslatableSubscriber;
use TranslationEntityBundle\Hydrator\ObjectHydrator;
use TranslationEntityBundle\Hydrator\SimpleObjectHydrator;

class TranslationWalker extends SqlWalker
{
    const HYDRATE_OBJECT_TRANSLATION = 'translations.hydrator.object';
    const HYDRATE_SIMPLE_OBJECT_TRANSLATION = 'translations.hydrator.simple_object';

    /**
     * @var TranslatableSubscriber
     */
    protected $listener;

    /**
     * @var array
     */
    protected $translatedComponents = array();

    /**
     * List of aliases to replace with translation
     * content reference
     *
     * @var array
     */
    private $replacements = array();

    /**
     * List of joins for translated components in query
     *
     * @var array
     */
    private $components = array();

    /**
     * @var AbstractPlatform
     */
    protected $platform;

    /** @var bool */
    protected $active = false;

    /**
     * TranslationWalker constructor.
     * @param $query
     * @param $parserResult
     * @param array $queryComponents
     * @throws
     */
    public function __construct($query, $parserResult, array $queryComponents)
    {
        parent::__construct($query, $parserResult, $queryComponents);
        $this->listener = $this->getTranslatableSubscriber();
        $this->platform = $this->getConnection()->getDatabasePlatform();
        if($this->listener){
            /** @var Query $query */
            $em = $query->getEntityManager();
            $this->extractTranslatedComponents($em, $queryComponents);
        }
    }

    /**
     * {@inheritDoc}
     * @throws
     */
    public function getExecutor($AST)
    {
        $this->active = ($AST instanceof SelectStatement) && count($this->translatedComponents);
        if($this->active){
            $this->prepareTranslatedComponents();
        }
        return parent::getExecutor($AST);
    }

    /**
     * {@inheritDoc}
     */
    public function walkSelectClause($selectClause)
    {
        $result = parent::walkSelectClause($selectClause);
        if($this->active){
            $result = $this->replace($this->replacements, $result);
        }
        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function walkFromClause($fromClause)
    {
        $result = parent::walkFromClause($fromClause);
        if($this->active){
            $result .= $this->joinTranslations($fromClause);
        }
        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function walkWhereClause($whereClause)
    {
        $result = parent::walkWhereClause($whereClause);
        if($this->active){
            $result = $this->replace($this->replacements, $result);
        }
        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function walkHavingClause($havingClause)
    {
        $result = parent::walkHavingClause($havingClause);
        if($this->active){
            $result = $this->replace($this->replacements, $result);
        }
        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function walkOrderByClause($orderByClause)
    {
        $result = parent::walkOrderByClause($orderByClause);
        if($this->active){
            $result = $this->replace($this->replacements, $result);
        }
        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function walkSubselectFromClause($subselectFromClause)
    {
        $result = parent::walkSubselectFromClause($subselectFromClause);
        if($this->active){
            $result .= $this->joinTranslations($subselectFromClause);
        }
        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function walkSimpleSelectClause($simpleSelectClause)
    {
        $result = parent::walkSimpleSelectClause($simpleSelectClause);
        if($this->active){
            $result = $this->replace($this->replacements, $result);
        }
        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function walkGroupByClause($groupByClause)
    {
        $result = parent::walkGroupByClause($groupByClause);
        if($this->active){
            $result = $this->replace($this->replacements, $result);
        }
        return $result;
    }

    /**
     * @param  \Doctrine\ORM\Query\AST\FromClause $from
     * @return string
     */
    private function joinTranslations($from)
    {
        $result = '';
        foreach ($from->identificationVariableDeclarations as $decl) {
            if ($decl->rangeVariableDeclaration instanceof RangeVariableDeclaration) {
                if (isset($this->components[$decl->rangeVariableDeclaration->aliasIdentificationVariable])) {
                    $result .= $this->components[$decl->rangeVariableDeclaration->aliasIdentificationVariable];
                }
            }
            if (isset($decl->joinVariableDeclarations)) {
                foreach ($decl->joinVariableDeclarations as $joinDecl) {
                    if ($joinDecl->join instanceof Join) {
                        if (isset($this->components[$joinDecl->join->aliasIdentificationVariable])) {
                            $result .= $this->components[$joinDecl->join->aliasIdentificationVariable];
                        }
                    }
                }
            } else {
                // based on new changes
                foreach ($decl->joins as $join) {
                    if ($join instanceof Join) {
                        if (isset($this->components[$join->joinAssociationDeclaration->aliasIdentificationVariable])) {
                            $result .= $this->components[$join->joinAssociationDeclaration->aliasIdentificationVariable];
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @throws
     */
    private function prepareTranslatedComponents()
    {
        $query = $this->getQuery();

        $em = $this->getEntityManager();
        $qs = $em->getConfiguration()->getQuoteStrategy();

        if($query->hasHint(TranslatableSubscriber::HINT_TRANS_LOCALE)){
            $locale = $query->getHint(TranslatableSubscriber::HINT_TRANS_LOCALE);
        } else {
            $locale = $this->listener->getCurrentLocale();
            $query->setHint(TranslatableSubscriber::HINT_TRANS_LOCALE, $locale);
        }

        $masterFallback = null;
        if($query->hasHint(TranslatableSubscriber::HINT_MASTER_FALLBACK)) {
            $masterFallback = (bool)$query->getHint(TranslatableSubscriber::HINT_MASTER_FALLBACK);
        }

        foreach ($this->translatedComponents as $dqlAlias => $comp)
        {
            /** @var ClassMetadata $meta */
            $meta = $comp['metadata'];
            $em = $this->getEntityManager();
            $config = $this->listener->getConfiguration($em, $meta);
            $fallback = isset($masterFallback) ? $masterFallback : $config['fallback'];
            $joinStrategy = $fallback ? 'LEFT' : 'INNER';
            $transMeta = $em->getClassMetadata($config['classTrans']);

            $table = $qs->getTableName($meta, $this->platform);
            $tableAlias = $this->getSQLTableAlias($table, $dqlAlias);
            $transTable = $qs->getTableName($transMeta, $this->platform);
            $transTableAlias = $this->getSQLTableAlias("{$transTable}_{$dqlAlias}_trans");

            $identifier = $meta->getSingleIdentifierFieldName();
            $identifierTrans = $transMeta->getSingleIdentifierFieldName();
            $idColName = $qs->getColumnName($identifier, $meta, $this->platform);
            $idColNameTrans = $qs->getColumnName($identifierTrans, $transMeta, $this->platform);

            $sql = " {$joinStrategy} JOIN ".$transTable.' '.$transTableAlias;
            $sql .= ' ON '.$tableAlias.'.'.$idColName.' = '.$transTableAlias.'.'.
                $transMeta->getSingleAssociationJoinColumnName('transObject');
            $sql .= ' AND '.$transTableAlias.'.'.$qs->getColumnName('transLocale', $transMeta, $this->platform)
                .' = '.$this->getConnection()->quote($locale);

            $this->components[$dqlAlias] = $sql;

            foreach ($config['fieldsTrans'] as $field) {
                $originalField = $tableAlias.'.'.$qs->getColumnName($field, $meta, $this->platform);
                $substituteField = $transTableAlias.'.'.$qs->getColumnName($field, $transMeta, $this->platform);
                if ($fallback) {
                    $substituteField = 'IF('.$transTableAlias.'.'.$idColNameTrans.' IS NOT NULL, '.$substituteField.', '.$originalField.')';
                }
                $this->replacements[$originalField] = $substituteField;
            }
        }
    }

    /**
     * Search for translated components in the select clause
     *
     * @param EntityManager $em
     * @param array $queryComponents
     * @throws
     */
    private function extractTranslatedComponents(EntityManager $em, array $queryComponents)
    {
        foreach ($queryComponents as $alias => $comp) {
            if (!isset($comp['metadata'])) {
                continue;
            }
            $metadata = $comp['metadata'];
            $config = $this->listener->getConfiguration($em, $metadata);
            if ($config && isset($config['fieldsTrans'])) {
                $this->translatedComponents[$alias] = $comp;
            }
        }
    }

    /**
     * @return TranslatableSubscriber
     * @throws
     */
    private function getTranslatableSubscriber()
    {
        $em = $this->getEntityManager();
        foreach ($em->getEventManager()->getListeners() as $event => $listeners) {
            foreach ($listeners as $hash => $listener) {
                if ($listener instanceof TranslatableSubscriber) {
                    return $listener;
                }
            }
        }
    }

    /**
     * Replaces given sql $str with required
     * results
     *
     * @param array  $repl
     * @param string $str
     *
     * @return string
     */
    private function replace(array $repl, $str)
    {
        foreach ($repl as $target => $result) {
            $str = preg_replace_callback('/(\s|\()('.$target.')(,?)(\s|\)|$)/smi', function ($m) use ($result) {
                return $m[1].$result.$m[3].$m[4];
            }, $str);
        }
        return $str;
    }
}
