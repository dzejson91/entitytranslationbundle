<?php
/**
 * Created by PhpStorm.
 * User: Krystian
 * Date: 26.09.2018
 * Time: 20:03
 */

namespace TranslationEntityBundle\DependencyInjection;

use TranslationEntityBundle\EventListener\TranslatableSubscriber;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Reference;

class TranslationEntityExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container){
        $container
            ->setDefinition('translation_entity.subscriber', new Definition(
                TranslatableSubscriber::class
            ))
            ->addTag('doctrine.event_subscriber');
    }
}