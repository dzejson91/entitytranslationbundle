<?php

/**
 * @author Krystian Jasnos <dzejson91@gmail.com>
 */

namespace TranslationEntityBundle;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use TranslationEntityBundle\Query\TranslationWalker;

/**
 * Class ORTEBundle
 * @package TranslationEntityBundle
 */
class TranslationEntityBundle extends Bundle
{
    public function boot()
    {
        parent::boot();

        /** @var Registry $doctrine */
        $doctrine = $this->container->get('doctrine');

        /** @var ObjectManager[] $managers */
        $managers = $doctrine->getManagers();
        foreach ($managers as $manager){
            if($manager instanceof EntityManager){
                $manager
                    ->getConfiguration()
                    ->setDefaultQueryHint(
                        Query::HINT_CUSTOM_OUTPUT_WALKER,
                        TranslationWalker::class
                    )
                ;
            }
        }
    }

}