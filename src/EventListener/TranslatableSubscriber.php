<?php
/**
 * Created by PhpStorm.
 * User: Krystian
 * Date: 26.09.2018
 * Time: 19:58
 */

namespace TranslationEntityBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query;
use MailBundle\Entity\Content;
use TranslationEntityBundle\Query\TranslationWalker;
use TranslationEntityBundle\Translation\AbstractEntityTranslation;
use TranslationEntityBundle\Translation\EntityTranslatableInterface;

class TranslatableSubscriber implements EventSubscriber
{
    const HINT_TRANS_LOCALE = 'translatable.locale';
    const HINT_MASTER_FALLBACK = 'translatable.fallback';

    const CLASS_METADATA_CACHE_SALT = '$CLASSMETADATA';

    /** @var array */
    protected $configs = array();

    /** @var string */
    protected $defaultLocale = 'en_US';

    /** @var string|null */
    protected $currentLocale;

    /**
     * @return array|string[]
     */
    public function getSubscribedEvents(){
        return array(
            Events::loadClassMetadata,
            Events::onFlush,
        );
    }

    /**
     * @param LoadClassMetadataEventArgs $args
     * @throws \Exception
     */
    public function loadClassMetaData(LoadClassMetadataEventArgs $args)
    {
        $metadata = $args->getClassMetadata();
        $valid = $this->validate($metadata);
        if($valid){
            $configuration = $this->getConfiguration($args->getEntityManager(), $metadata);
            $this->dynamicMapping($args->getEntityManager(), $metadata, $configuration);
        }
    }

    /**
     * @param OnFlushEventArgs  $args
     * @throws
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $object) {
            $metadata = $em->getClassMetadata(get_class($object));
            $config = $this->getConfiguration($args->getEntityManager(), $metadata);
            if ($config && !empty($config['fieldsTrans'])) {
                $this->handleTranslatableObjectUpdate($em, $object, true);
            }
        }

        foreach ($uow->getScheduledEntityUpdates() as $object) {
            $metadata = $em->getClassMetadata(get_class($object));
            $config = $this->getConfiguration($args->getEntityManager(), $metadata);
            if ($config && !empty($config['fieldsTrans'])) {
                $this->handleTranslatableObjectUpdate($em, $object, false);
            }
        }
    }

    /**
     * @param ClassMetadata $metadata
     * @return bool
     * @throws
     */
    protected function validate(ClassMetadata $metadata){

        /** @var EntityTranslatableInterface $class */
        $class = $metadata->getName();

        if($metadata->isMappedSuperclass){
            return false;
        }

        if(!in_array(EntityTranslatableInterface::class, class_implements($class))){
            return false;
        }

        /** @var AbstractEntityTranslation $classTrans */
        $classTrans = $class::getTranslatableClass();

        if(!in_array(AbstractEntityTranslation::class, class_parents($classTrans))){
            throw new \Exception(sprintf('Class %s must extends %s', $classTrans, AbstractEntityTranslation::class));
        }

        return true;
    }

    /**
     * @param EntityManager $em
     * @param ClassMetadata $metadata
     * @return mixed
     * @throws \Exception
     */
    public function getConfiguration(EntityManager $em, ClassMetadata $metadata)
    {
        $class = $metadata->getName();

        if(isset($this->configs[$class])){
            return $this->configs[$class];
        }

        if($metadata->isMappedSuperclass){
            return $this->configs[$class] = false;
        }

        if(!in_array(EntityTranslatableInterface::class, class_implements($class))){
            return $this->configs[$class] = false;
        }

        /** @var EntityTranslatableInterface $class */
        /** @var AbstractEntityTranslation $classTrans */
        $classTrans = $class::getTranslatableClass();
        $metadataTrans = $em->getClassMetadata($classTrans);

        $dynamicFieldNames = array();

        $fieldsTrans = array('transId', 'transObject', 'transLocale');
        $propertiesTrans = $metadataTrans->getReflectionClass()->getProperties();
        foreach($propertiesTrans as $propertyTrans){
            $name = $propertyTrans->getName();
            if(in_array($name, $fieldsTrans)){
                continue;
            }
            if($metadata->hasField($name)){
                $dynamicFieldNames[] = $name;
            }
        }

        $config = array(
            'classTrans' => $classTrans,
            'fieldsTrans' => $dynamicFieldNames,
            'fallback' => $classTrans::isFallback(),
        );

        $this->configs[$class] = $config;
        return $config;
    }

    /**
     * @param EntityManager $em
     * @param ClassMetadata $metadata
     * @param array $configuration
     * @throws
     */
    protected function dynamicMapping(EntityManager $em, ClassMetadata $metadata, array $configuration = array()){
        $class = $metadata->getName();

        $classTrans = $configuration['classTrans'];
        $fieldsTrans = $configuration['fieldsTrans'];

        $factory = $em->getMetadataFactory();

        /** @var ClassMetadata $metadataTrans */
        $metadataTrans = $factory->getMetadataFor($classTrans);

        // if metadata load from cache
        $ns = $em->getConfiguration()->getNamingStrategy();
        $metadataTrans->__construct($metadataTrans->getName(), $ns);

        foreach($fieldsTrans as $propertyName){
            if($metadataTrans->hasField($propertyName)){
                continue;
            }
            $mapping = $metadata->getFieldMapping($propertyName);
            if($mapping['unique']){
                $mapping['unique'] = false;

                $uniqueColumns = array(
                    $ns->propertyToColumnName($mapping['fieldName']),
                    $ns->propertyToColumnName('transLocale'),
                );
                $metadataTrans->table['uniqueConstraints'][]['columns'] = $uniqueColumns;
            }
            $metadataTrans->mapField($mapping);
            $this->addReflectionPropertyIfNotExists($em, $metadataTrans, $propertyName);
        }

        if(!$metadata->hasAssociation('translations')){

            $metadataTrans->table['indexes'][]['columns'] = array(
                $ns->joinColumnName('transObject'),
                $ns->propertyToColumnName('transLocale'),
            );

            $this->addReflectionPropertyIfNotExists($em, $metadataTrans, 'transObject');

            if(!$metadata->hasAssociation('translations')){
                $metadata->mapOneToMany(array(
                    'targetEntity' => $classTrans,
                    'fieldName' => 'translations',
                    'mappedBy' => 'transObject',
                    'cascade' => array('persist'),
                ));
            }

            if(!$metadataTrans->hasAssociation('transObject')){
                $metadataTrans->mapManyToOne(array(
                    'targetEntity' => $class,
                    'fieldName' => 'transObject',
                    'inversedBy' => 'translations',
                    'joinColumns' => array(
                        array(
                            'nullable' => false,
                            'onDelete' => 'CASCADE',
                        )
                    ),
                ));
            }
        }

        // save metadata changes
        $cacheDriver = $em->getMetadataFactory()->getCacheDriver();
        $cacheDriver->save($metadata->getName() . self::CLASS_METADATA_CACHE_SALT, $metadata);
        $cacheDriver->save($metadataTrans->getName() . self::CLASS_METADATA_CACHE_SALT, $metadataTrans);
    }

    /**
     * @param EntityManager $em
     * @param ClassMetadata $metadata
     * @param $propertyName
     */
    protected function addReflectionPropertyIfNotExists(EntityManager $em, ClassMetadata $metadata, $propertyName){
        if(array_key_exists($propertyName, $metadata->reflFields)) return;
        $reflectionService = $em->getMetadataFactory()->getReflectionService();
        $metadata->reflFields[$propertyName] = $reflectionService->getAccessibleProperty(
            $metadata->getName(),
            $propertyName
        );
    }

    /**
     * @param \stdClass $object
     * @return bool
     */
    protected function checkEntityTranslatable($object){
        return $object instanceof EntityTranslatableInterface;
    }

    /**
     * @param EntityManager $em
     * @param EntityTranslatableInterface $object
     * @param boolean $isInsert
     * @throws
     */
    private function handleTranslatableObjectUpdate(EntityManager $em, EntityTranslatableInterface $object, $isInsert)
    {
        /** @var EntityTranslatableInterface $object */

        $metadata = $em->getClassMetadata(get_class($object));
        $uow = $em->getUnitOfWork();

        $config = $this->getConfiguration($em, $metadata);

        $classTrans = $config['classTrans'];
        $fieldsTrans = $config['fieldsTrans'];
        $metadataTrans = $em->getClassMetadata($classTrans);

        $locale = $this->getCurrentLocale();

        if($isInsert){
            $translation = null;
        } else {
            /** @var AbstractEntityTranslation $translation */
            $translation = $object->getTranslationByLocale($locale);
        }

        $translationExists = $translation && true;
        if(!$translationExists){
            $translation = $metadataTrans->newInstance();
            $translation->setTransObject($object);
            $translation->setTransLocale($locale);
        }

        $differentLocale = $this->getDefaultLocale() != $this->getCurrentLocale();
        $reflProperties = $metadataTrans->getReflectionProperties();
        $changeSet = $uow->getEntityChangeSet($object);

        foreach ($fieldsTrans as $fieldName){
            if(!$translationExists || isset($changeSet[$fieldName])){
                $value = $metadata->getFieldValue($object, $fieldName);
                $this->addReflectionPropertyIfNotExists($em, $metadataTrans, $fieldName);
                $metadataTrans->setFieldValue($translation, $fieldName, $value);
            }
            if($differentLocale && isset($changeSet[$fieldName])){
                $metadata->setFieldValue($object, $fieldName, reset($changeSet[$fieldName]));
            }
        }

        if($differentLocale){
            $uow->recomputeSingleEntityChangeSet($metadata, $object);
        }

        $em->persist($translation);
        $uow->computeChangeSet($metadataTrans, $translation);
    }

    /**
     * @return string
     */
    public function getDefaultLocale()
    {
        return $this->defaultLocale;
    }

    /**
     * @param string $defaultLocale
     */
    public function setDefaultLocale($defaultLocale)
    {
        $this->defaultLocale = $defaultLocale;
    }

    /**
     * @return string
     */
    public function getCurrentLocale()
    {
        if(!isset($this->currentLocale)){
            return $this->getDefaultLocale();
        }
        return $this->currentLocale;
    }

    /**
     * @param string $currentLocale
     */
    public function setCurrentLocale($currentLocale)
    {
        $this->currentLocale = $currentLocale;
    }

    /**
     * @return bool
     */
    public function isSkipOnLoad()
    {
        return $this->skipOnLoad;
    }

    /**
     * @param bool $skipOnLoad
     */
    public function setSkipOnLoad($skipOnLoad)
    {
        $this->skipOnLoad = $skipOnLoad;
    }
}