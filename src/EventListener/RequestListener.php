<?php
/**
 * Created by PhpStorm.
 * User: Krystian
 * Date: 12.11.2018
 * Time: 16:08
 */

namespace TranslationEntityBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RequestListener
{
    /** @var TranslatableSubscriber $entityTransService */
    protected $entityTransService;
    
    /**
     * RequestListener constructor.
     *
     * @var TranslatableSubscriber $entityTransService
     * @var bool $onlyFirstRequest
     */
    public function __construct(TranslatableSubscriber $entityTransService)
    {
        $this->entityTransService = $entityTransService;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $this->entityTransService->setDefaultLocale($request->getDefaultLocale());
        $this->entityTransService->setCurrentLocale($request->getLocale());
    }
}